/*
    This file is part of KDE
    SPDX-FileCopyrightText: 2023 Nicolas Fella <nicolas.fella@gmx.de>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include <KIO/TransferJob>

#include <QSignalSpy>
#include <QTest>

class MimeTypeTest : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void testNoAuthPrompt();
    void testNoAuthPrompt_data();
};

void MimeTypeTest::testNoAuthPrompt_data()
{
    QTest::addColumn<QString>("url");
    QTest::addColumn<QString>("expectedMimeType");
    QTest::addColumn<QByteArray>("expectedData");

    QTest::addRow("html") << "http://localhost:5000/auth/test"
                          << "text/html" << QByteArray("Hello");
}

void MimeTypeTest::testNoAuthPrompt()
{
    QFETCH(QString, url);
    QFETCH(QString, expectedMimeType);
    QFETCH(QByteArray, expectedData);

    auto *job = KIO::get(QUrl(url));

    job->addMetaData("no-auth-prompt", "true");

    QSignalSpy dataSpy(job, &KIO::TransferJob::data);
    QSignalSpy spy(job, &KJob::finished);
    spy.wait();
    QVERIFY(spy.size());

    // QVERIFY(dataSpy.count());
    // const QByteArray actualData = dataSpy.first().at(1).toByteArray();
    // QCOMPARE(actualData, expectedData);

    qWarning() << "error" << job->error();

    QCOMPARE(job->error(), KJob::NoError);
}

QTEST_GUILESS_MAIN(MimeTypeTest)

#include "noauthtest.moc"
