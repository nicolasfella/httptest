/*
    This file is part of KDE
    SPDX-FileCopyrightText: 2023 Nicolas Fella <nicolas.fella@gmx.de>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include <QApplication>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <KIO/SslUi>

int main(int argc, char** argv) {

    QNetworkAccessManager nam;

    QApplication app(argc, argv);

    auto reply = nam.get(QNetworkRequest(QUrl("https://expired.badssl.com/")));

    QObject::connect(reply, &QNetworkReply::sslErrors, &app, [reply](const QList<QSslError> &errors){
        qWarning() << "errors" << errors.first();

        KSslErrorUiData errorData(reply, errors);
        if (KIO::SslUi::askIgnoreSslErrors(errorData)) {
            reply->ignoreSslErrors();
        }
    });

    QObject::connect(reply, &QNetworkReply::finished, &app, [reply]{
        qWarning() << "done" << reply->error();
    });

    return app.exec();
}
