/*
    This file is part of KDE
    SPDX-FileCopyrightText: 2023 Nicolas Fella <nicolas.fella@gmx.de>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include <QApplication>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <KIO/TransferJob>

#include <KIO/SslUi>

int main(int argc, char** argv) {

    QApplication app(argc, argv);

    auto job = KIO::get(QUrl("httpng://localhost:5000/get/redirect"));

    QObject::connect(job, &KIO::TransferJob::redirection, &app, [](KJob *job, const QUrl &url) {
        qWarning() << "redirect to" << url;
    });

    QObject::connect(job, &KJob::finished, &app, [job]{
        qWarning() << "done" << job->error();
    });

    return app.exec();
}
