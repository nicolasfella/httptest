/*
    This file is part of KDE
    SPDX-FileCopyrightText: 2023 Nicolas Fella <nicolas.fella@gmx.de>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include <QApplication>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <KIO/TransferJob>

#include <QSignalSpy>
#include <QTest>

class MimeTypeTest : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void testGet();
};

void MimeTypeTest::testGet()
{
    auto *job = KIO::get(QUrl("http://localhost:5000/does_not_exist"));

    QSignalSpy spy(job, &KJob::finished);
    spy.wait();
    QVERIFY(spy.size());
    QCOMPARE(job->error(), KJob::NoError);
    QCOMPARE(job->isErrorPage(), true);
}

QTEST_GUILESS_MAIN(MimeTypeTest)

#include "404test.moc"
